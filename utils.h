#ifndef NETWORKS_UTILS_H
#define NETWORKS_UTILS_H

#include <math.h>
namespace networks
{
inline float sigmoid(float x)
{
    return static_cast<float>(1) / (static_cast<float>(1) + exp(-x));
}

} // namespace networks

#endif // UTILS_H
