#ifndef NETWORKS_LAYER_H
#define NETWORKS_LAYER_H

#include "neuron.h"
#include <vector>

namespace networks
{

struct Layer {
    using WeightMatrix = std::vector<std::vector<float>>;

    Layer(size_t neuronAmount);
    Layer(size_t neuronAmount, size_t previousLayerNeuronsAmount);

    void connect(Layer const &previous);
    std::vector<float>
    applyNeuronProcessing(std::vector<float> const &data) const;

    WeightMatrix getCorrectionsWrtErrors(
        std::vector<float> const &errors,
        const std::vector<float> &previousLayerResults) const;

    float getCorrectionsWrtValues(std::vector<float> const &errors,
                                  size_t index) const;

    void makeCorrections(std::vector<std::vector<float>> const &derivative,
                         float coefficient);

private:
    void initWeightMatrix();

    size_t neuronAmount;
    size_t previousLayerNeuronsAmount;
    std::vector<Neuron> neurons;
    WeightMatrix weightMatrix;
};

} // namespace networks

#endif
