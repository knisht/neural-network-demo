#include "neuralnetwork.h"
#include <iostream>

using namespace std;
using namespace networks;

void printVector(std::vector<float> res)
{
    std::cout << "{";
    for (size_t i = 0; i < res.size(); ++i) {
        std::cout << res[i] << ((i != res.size() - 1) ? ", " : "}");
    }
}

void evaluate(std::vector<float> const &input, std::vector<float> const &expect,
              std::vector<float> const &result)
{
    std::cout << "Try ";
    printVector(input);
    std::cout << " (expected ";
    printVector(expect);
    std::cout << "): got ";
    printVector(result);
    std::cout << std::endl;
}

void exampleAndNetwork()
{
    std::cout << "==== Neural Network sample for boolean AND ====" << std::endl;
    NeuralNetwork network;
    network.addLayer(Layer(1, 2));
    std::vector<std::vector<float>> train_data{{1, 0}, {0, 1}, {0, 0}, {1, 1}};
    std::vector<std::vector<float>> train_label = {{0}, {0}, {0}, {1}};
    for (size_t i = 0; i < 1000; ++i) {
        for (size_t j = 0; j < train_data.size(); ++j) {
            network.trainSingleSample(train_data[j], train_label[j], 0.1);
        }
    }
    evaluate({0, 0}, {0}, network.evaluate({0, 0}));
    evaluate({0, 1}, {0}, network.evaluate({0, 1}));
    evaluate({1, 0}, {0}, network.evaluate({1, 0}));
    evaluate({1, 1}, {1}, network.evaluate({1, 1}));
    std::cout << "===============================================" << std::endl
              << std::endl;
}

void exampleOrNetwork()
{
    std::cout << "==== Neural Network sample for boolean OR =====" << std::endl;
    NeuralNetwork network;
    network.addLayer(Layer(1, 2));
    std::vector<std::vector<float>> train_data{{1, 0}, {0, 1}, {0, 0}, {1, 1}};
    std::vector<std::vector<float>> train_label = {{1}, {1}, {0}, {1}};
    for (size_t i = 0; i < 1000; ++i) {
        for (size_t j = 0; j < train_data.size(); ++j) {
            network.trainSingleSample(train_data[j], train_label[j], 0.1);
        }
    }
    evaluate({0, 0}, {0}, network.evaluate({0, 0}));
    evaluate({0, 1}, {1}, network.evaluate({0, 1}));
    evaluate({1, 0}, {1}, network.evaluate({1, 0}));
    evaluate({1, 1}, {1}, network.evaluate({1, 1}));
    std::cout << "===============================================" << std::endl
              << std::endl;
}

void exampleXorNetwork()
{
    std::cout << "==== Neural Network sample for boolean XOR ====" << std::endl;
    NeuralNetwork network;
    network.addLayer(Layer(10, 2));
    network.addLayer(Layer(1));
    std::vector<std::vector<float>> train_data{{1, 0}, {0, 1}, {0, 0}, {1, 1}};
    std::vector<std::vector<float>> train_label = {
        {1, 0}, {1, 0}, {0, 1}, {0, 1}};
    for (size_t i = 0; i < 5000; ++i) {
        for (size_t j = 0; j < train_data.size(); ++j) {
            network.trainSingleSample(train_data[j], train_label[j], 0.1);
        }
    }
    evaluate({0, 0}, {0}, network.evaluate({0, 0}));
    evaluate({0, 1}, {1}, network.evaluate({0, 1}));
    evaluate({1, 0}, {1}, network.evaluate({1, 0}));
    evaluate({1, 1}, {0}, network.evaluate({1, 1}));
    std::cout << "===============================================" << std::endl
              << std::endl;
}

int main()
{
    exampleAndNetwork();
    exampleOrNetwork();
    exampleXorNetwork();
    return 0;
}
