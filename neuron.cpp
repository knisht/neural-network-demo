#include "neuron.h"

namespace networks
{

Neuron::Neuron(std::function<float(float)> f) : activationFunction(f) {}

float Neuron::operator()(float x) const { return activationFunction(x); }

} // namespace networks
