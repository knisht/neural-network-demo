#include "layer.h"
#include "utils.h"
#include <assert.h>
#include <random>

namespace networks
{

Layer::Layer(size_t neuronAmount)
    : neuronAmount(neuronAmount), previousLayerNeuronsAmount(0),
      neurons(neuronAmount, Neuron(sigmoid))
{
    assert(neuronAmount > 0 && "Neuron amount must be positive");
}

Layer::Layer(size_t neuronAmount, size_t previousNeuronsAmount)
    : neuronAmount(neuronAmount),
      previousLayerNeuronsAmount(previousNeuronsAmount),
      neurons(neuronAmount, Neuron(sigmoid))
{
    assert(neuronAmount > 0 && "Neuron amount must be positive");
    initWeightMatrix();
}

void Layer::initWeightMatrix()
{
    assert(previousLayerNeuronsAmount > 0 && "Unknown previous layer size");
    std::mt19937 rng;
    rng.seed(std::random_device()());
    std::uniform_real_distribution<float> dist(-0.5, 0.5);
    weightMatrix = {neuronAmount,
                    std::vector<float>(previousLayerNeuronsAmount + 1, 0)};
    for (size_t i = 0; i < weightMatrix.size(); ++i) {
        for (size_t j = 0; j < weightMatrix[i].size(); ++j) {
            weightMatrix[i][j] = dist(rng);
        }
    }
}

void Layer::connect(const Layer &previous)
{
    previousLayerNeuronsAmount = previous.neuronAmount;
    initWeightMatrix();
}

std::vector<float>
Layer::applyNeuronProcessing(std::vector<float> const &data) const
{
    assert((data.size() == previousLayerNeuronsAmount) &&
           "Incorrect input data dimension");
    assert(previousLayerNeuronsAmount > 0 &&
           "Layer is not properly configured");
    std::vector<float> results(neuronAmount);
    for (size_t i = 0; i < neuronAmount; ++i) {
        float accumulator = 1 * weightMatrix[i][0];
        for (size_t j = 0; j < previousLayerNeuronsAmount; ++j) {
            accumulator += weightMatrix[i][j + 1] * data[j];
        }
        results[i] = neurons[i](accumulator);
    }
    return results;
}

Layer::WeightMatrix Layer::getCorrectionsWrtErrors(
    std::vector<float> const &errors,
    std::vector<float> const &previousLayerResults) const
{
    assert(errors.size() == neuronAmount &&
           "Expected the same dimension of error vector and neuron vector");
    assert(previousLayerResults.size() == previousLayerNeuronsAmount &&
           "Expected the same dimension of previous level neuron vector and "
           "previous level prediction vector");
    WeightMatrix result(weightMatrix.size(),
                        std::vector<float>(previousLayerNeuronsAmount + 1, 0));
    for (size_t i = 0; i < result.size(); ++i) {
        result[i][0] = errors[i];
        for (size_t j = 1; j < result[i].size(); ++j) {
            result[i][j] = errors[i] * previousLayerResults[j - 1];
        }
    }
    return result;
}

float Layer::getCorrectionsWrtValues(const std::vector<float> &errors,
                                     size_t index) const
{
    assert(errors.size() == neuronAmount &&
           "Expected the same dimension of error vector and neuron vector");
    assert(index < previousLayerNeuronsAmount + 1 &&
           "Expected index in range of previous level neuron amount");
    float accumulator = 0;
    for (size_t j = 0; j < neuronAmount; ++j) {
        accumulator += errors[j] * weightMatrix[j][index + 1];
    }
    return accumulator;
}

void Layer::makeCorrections(std::vector<std::vector<float>> const &derivative,
                            float coefficient)
{
    assert(derivative.size() == weightMatrix.size() &&
           derivative[0].size() == weightMatrix[0].size() &&
           "Expected derivative matrix the same dimension as weight matrix");
    for (size_t i = 0; i < weightMatrix.size(); ++i) {
        for (size_t j = 0; j < weightMatrix[i].size(); ++j) {
            weightMatrix[i][j] -= derivative[i][j] * coefficient;
        }
    }
}

} // namespace networks
