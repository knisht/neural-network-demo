#include "neuralnetwork.h"
#include <assert.h>

namespace networks
{

NeuralNetwork::NeuralNetwork() {}

void NeuralNetwork::addLayer(const Layer &layer)
{
    layers.push_back(layer);
    if (layers.size() > 1) {
        layers.back().connect(layers[layers.size() - 2]);
    }
}

void NeuralNetwork::moveLayer(Layer &&layer)
{
    layers.push_back(layer);
    if (layers.size() > 0) {
        layers.back().connect(layers[layers.size() - 1]);
    }
}

void NeuralNetwork::trainSingleSample(const std::vector<float> &trainData,
                                      const std::vector<float> &labels,
                                      float learningRate)
{
    assert(layers.size() > 0 && "Network has no layers");
    std::vector<std::vector<float>> networkState =
        performFeedForward(trainData);
    std::vector<float> processedResult = networkState.back();
    for (size_t i = 0; i < processedResult.size(); ++i) {
        processedResult[i] -= labels[i];
    }
    std::vector<std::vector<std::vector<float>>> derivative =
        performBackPropagation(processedResult, trainData, networkState);
    for (size_t i = 0; i < derivative.size(); ++i) {
        layers[i].makeCorrections(derivative[i], learningRate);
    }
}

void NeuralNetwork::trainMultipleSamples(
    const std::vector<std::vector<float>> &multipleTrainData,
    const std::vector<std::vector<float>> &multipleTrainLabels,
    float learningRate)
{
    assert(multipleTrainData.size() == multipleTrainLabels.size() &&
           "Train data should have the same dimension");
    for (size_t i = 0; i < multipleTrainData.size(); ++i) {
        trainSingleSample(multipleTrainData[i], multipleTrainLabels[i],
                          learningRate);
    }
}

std::vector<float>
NeuralNetwork::evaluate(const std::vector<float> &test_data) const
{
    return performFeedForward(test_data).back();
}

float NeuralNetwork::computeErrorFunction(
    const std::vector<float> &testData, std::vector<float> const &labels) const
{
    std::vector<float> results = evaluate(testData);
    float accum = 0;
    for (size_t i = 0; i < results.size(); ++i) {
        accum += (results[i] - labels[i]) * (results[i] - labels[i]);
    }
    return accum;
}

std::vector<std::vector<float>>
NeuralNetwork::performFeedForward(std::vector<float> const &train_data) const
{
    std::vector<std::vector<float>> intermediateResults(layers.size());
    for (size_t i = 0; i < layers.size(); ++i) {
        intermediateResults[i] = layers[i].applyNeuronProcessing(
            (i == 0) ? train_data : intermediateResults[i - 1]);
    }
    return intermediateResults;
}

std::vector<std::vector<std::vector<float>>>
NeuralNetwork::performBackPropagation(
    std::vector<float> const &lastLayerErrors,
    std::vector<float> const &train_data,
    std::vector<std::vector<float>> layerPredictions)
{
    std::vector<std::vector<std::vector<float>>> derivativeMatrix(
        layers.size());
    std::vector<float> errors(lastLayerErrors);
    derivativeMatrix.back() = layers.back().getCorrectionsWrtErrors(
        errors, layers.size() == 1
                    ? train_data
                    : layerPredictions[layerPredictions.size() - 2]);
    for (size_t layerIndex = layers.size() - 2; layerIndex < layers.size();
         --layerIndex) {
        std::vector<float> currentErrors(layerPredictions[layerIndex].size(),
                                         0);
        for (size_t i = 0; i < layerPredictions[layerIndex].size(); ++i) {
            float accumulator =
                layers[layerIndex + 1].getCorrectionsWrtValues(errors, i);
            currentErrors[i] = accumulator * layerPredictions[layerIndex][i] *
                               (1 - layerPredictions[layerIndex][i]);
        }
        errors = currentErrors;
        derivativeMatrix[layerIndex] =
            layers[layerIndex].getCorrectionsWrtErrors(
                errors, layerIndex == 0 ? train_data
                                        : layerPredictions[layerIndex - 1]);
    }
    return derivativeMatrix;
}

} // namespace networks
