#ifndef NETWORKS_NEURALNETWORK_H
#define NETWORKS_NEURALNETWORK_H
#include "layer.h"
#include <vector>

namespace networks
{

class NeuralNetwork
{
public:
    NeuralNetwork();

    void addLayer(Layer const &);
    void moveLayer(Layer &&);
    void trainSingleSample(std::vector<float> const &trainData,
                           std::vector<float> const &trainLabels,
                           float learningRate);
    void trainMultipleSamples(
        std::vector<std::vector<float>> const &multipleTrainData,
        std::vector<std::vector<float>> const &multipleTrainLabels,
        float learningRate);
    std::vector<float> evaluate(std::vector<float> const &test_data) const;
    float computeErrorFunction(std::vector<float> const &test_data,
                               std::vector<float> const &labels) const;

private:
    std::vector<std::vector<float>>
    performFeedForward(std::vector<float> const &train_data) const;
    std::vector<std::vector<std::vector<float>>>
    performbackPropagarion(std::vector<float> const &result_data,
                           const std::vector<float> &train_data,
                           std::vector<std::vector<float>> layerProductions);

    std::vector<Layer> layers;
};

} // namespace networks

#endif // NEURALNETWORK_H
