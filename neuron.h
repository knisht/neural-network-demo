#ifndef NETWORKS_NEURON_H
#define NETWORKS_NEURON_H
#include <functional>
#include <memory>

namespace networks
{
struct Neuron {
    Neuron(std::function<float(float)>);

    float operator()(float x) const;

private:
    std::function<float(float)> activationFunction;
};
} // namespace networks

#endif
